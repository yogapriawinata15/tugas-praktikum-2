package com.yogapriawinata_10181079.praktikum2.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.yogapriawinata_10181079.praktikum2.Fragment.HomeFragment;
import com.yogapriawinata_10181079.praktikum2.Fragment.SecondFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabsCount;

    public FragmentAdapter(Context context, int tabsCount, FragmentManager fragmentManager) {
        super(fragmentManager);

        this.context = context;
        this.tabsCount = tabsCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                SecondFragment secondFragment = new SecondFragment();
                return secondFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsCount;
    }
}
